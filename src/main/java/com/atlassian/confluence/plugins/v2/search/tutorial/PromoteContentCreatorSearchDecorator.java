package com.atlassian.confluence.plugins.v2.search.tutorial;

import com.atlassian.confluence.search.v2.HightlightParams;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.ResultFilter;
import com.atlassian.confluence.search.v2.SearchDecorator;
import com.atlassian.confluence.search.v2.SearchFilter;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ConstantScoreQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class PromoteContentCreatorSearchDecorator implements SearchDecorator {
    private final UserManager userManager;

    @Autowired
    public PromoteContentCreatorSearchDecorator(@ComponentImport UserManager userManager) {
        this.userManager = requireNonNull(userManager);
    }

    @Override
    public ISearch decorate(ISearch search) {
        UserKey loginUserKey = userManager.getRemoteUserKey();
        if (loginUserKey != null) {
            return new PromoteContentCreatorSearch(search, loginUserKey);
        }
        return search;
    }

    private static class PromoteContentCreatorSearch implements ISearch {
        private final ISearch delegate;
        private final UserKey creator;

        private PromoteContentCreatorSearch(ISearch delegate, UserKey creator) {
            this.delegate = delegate;
            this.creator = creator;
        }

        @Override
        public SearchQuery getQuery() {
            // a content matching should clause adds an overwhelming contribution to the overall score so it
            // is ranked before any unmatched content
            return BooleanQuery.builder()
                    .addMust(delegate.getQuery())
                    .addShould(new ConstantScoreQuery(new CreatorQuery(creator), 50.0f))
                    .build();
        }

        @Override
        public SearchSort getSort() {
            return delegate.getSort();
        }

        @Override
        public SearchFilter getSearchFilter() {
            return delegate.getSearchFilter();
        }

        @Override
        public ResultFilter getResultFilter() {
            return delegate.getResultFilter();
        }

        @Override
        public int getStartOffset() {
            return delegate.getStartOffset();
        }

        @Override
        public int getLimit() {
            return delegate.getLimit();
        }

        @Override
        public String getSearchType() {
            return delegate.getSearchType();
        }

        @Override
        public Optional<HightlightParams> getHighlight() {
            return delegate.getHighlight();
        }

        @Override
        public boolean isExplain() {
            return delegate.isExplain();
        }
    }
}
